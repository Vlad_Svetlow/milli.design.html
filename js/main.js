  var $header = $('#header'),
      $mainNav = $('#mainNav'),
      $banner = $('#banner'),
      $sliderFeatures = $('#sliderFeatures'),
      $sliderControls = $('#sliderControls'),
      $preloader = $('#pagePreloader'),
      $spinner   = $preloader.find('.bPreloaderContent');


function onScroll(event){
  var scrollPos = $(document).scrollTop();

  $mainNav.find('a').each(function () {
      var currLink = $(this);

      var refElement = $(currLink.attr("href"));

      if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {

          $mainNav.find('li').removeClass("active");
          currLink.parent('li').addClass("active");

      }
      else{
          currLink.parent('li').removeClass("active");
      }
  });
}

$(function(){

  //WoW JS Init
  wow = new WOW({
    boxClass:     'wow',      // default
    animateClass: 'animated', // default
    offset:       0,          // default
    mobile:       false,      
    live:         true        // default
  });

  //Sticky Header
  // function stickyHeader () {
  //   if ($(window).scrollTop() > 0) {
  //     $header.addClass('mSticky');
  //   } else {
  //     $header.removeClass('mSticky');
  //   }
  // }

  // stickyHeader();

  
  // $(window).on('scroll', function() {
  //   stickyHeader();
  // });

  //Preloader
  $(window).on('load', function () {
    $spinner.fadeOut();
    $preloader.delay(350).fadeOut('slow');

    setTimeout( function(){
      wow.init();
    }, 500);

  });

  //Section Banner full height
  $(window).on('load resize', function() {
    $banner.css({'height' : $(window).height() + 'px'});
  });

  //Owl Slider Features
  $sliderFeatures.owlCarousel({
    singleItem: true,
    mouseDrag: false,
    navigation: false,
    pagination: false,
    // afterMove: function() {
    //   console.log('test');
    // }
  });

  $sliderControls.find('.eNext').on('click', function() {
    $sliderFeatures.trigger('owl.next');
  });

  $sliderControls.find('.ePrev').on('click', function() {
    $sliderFeatures.trigger('owl.prev');
  });


  //Smoothscroll
   $(document).on("scroll", onScroll);
    
    $('#mainNav a[href^="#"]').on('click', function (e) {

        e.preventDefault();
        $(document).off("scroll");
        
        $('a').each(function () {
            $(this).removeClass('active');
        })
        $(this).addClass('active');
      
        var target = this.hash,
            menu = target;
        $target = $(target);

        $('html, body').stop().animate({
            'scrollTop': $target.offset().top+2
        }, 500, 'swing', function () {
            window.location.hash = target;
            $(document).on("scroll", onScroll);
        });

    });

});// End Doc Ready